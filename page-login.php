<?php get_header(); ?>
    <form id="login_form">
        <p><input type="text" id="login_form_login" placeholder="Login"></p>
        <p><input type="password" id="login_form_password" placeholder="Password"></p>
        <p><button type="submit">login</button></p>
    </form>
    <script>
        jQuery(document).ready(function ($) {
            $('#login_form').submit(function () {
                let login = $('#login_form_login').val();
                let password = $('#login_form_password').val();

                var settings = {
                    "async": true,
                    "crossDomain": true,
                    "url": "/wp-json/user-notes/login?login=" + login + "&password=" + password,
                    "method": "POST"
                };

                $.ajax(settings).done(function (response) {
                    let data = JSON.parse(response);

                    if (data.error == false) {
                        location.reload();
                    }
                });

                return false;
            });
        });
    </script>
<?php get_footer(); ?>