<?php

require_once('vendor/autoload.php');

$app = new \CactusModule\Bootstrap([
    'acfTheme'      => \MaterialTheme\Classes\Acf::class,
    'products'      => \MaterialTheme\Classes\Products::class,
    \MaterialTheme\Classes\RestApi::class,
    'materialTheme' => \MaterialTheme\Classes\Theme::class,
]);

$userNotes = \CactusModule\Bootstrap::get('MaterialTheme\Models\UserNotes123');
