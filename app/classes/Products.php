<?php

namespace MaterialTheme\Classes;

/**
 * Class Products
 * @package MaterialTheme\Classes
 */
class Products
{
    const POST_TYPE_SLUG = 'products';

    /**
     * @var array
     */
    public $fields_key = [
        'field_5d0b7c42a1e18',
        'field_5d0b7b4bb9ed0',
        'field_5d0b7c5ea1e19',
        'field_5d0b7b81b9ed1',
        'field_5d0b7ba1b9ed2',
        'field_5d0b7c76a1e1a',
        'field_5d0b7bbcb9ed3',
        'field_5d0b7bd5b9ed4',
    ];

    /**
     * Products constructor.
     */
    public function __construct()
    {
        add_action('init', [$this, 'registerProducts']);
        add_action('acf/init', [$this, 'productsAcfFields']);

        $this->saveActions();
    }

    /**
     * Register custom post type
     */
    public function registerProducts()
    {
        register_post_type(self::POST_TYPE_SLUG, [
            'labels' => [
                'name'               => 'Товары',
                'singular_name'      => 'Товар',
                'add_new'            => 'Добавить товар',
                'add_new_item'       => 'Добавить товар',
                'edit_item'          => 'Редактировать товар',
                'new_item'           => 'Новый товар',
                'view_item'          => 'Посмотреть товар',
                'search_items'       => 'Найти товар',
                'not_found'          =>  'Товаров не найдено',
                'not_found_in_trash' => 'В корзине товаров не найдено',
                'menu_name'          => 'Товары'
            ],
            'public'             => true,
            'publicly_queryable' => true,
            'show_ui'            => true,
            'show_in_menu'       => true,
            'query_var'          => true,
            'rewrite'            => true,
            'capability_type'    => 'post',
            'has_archive'        => true,
            'hierarchical'       => false,
            'menu_position'      => null,
            'supports'           => array('title')
        ]);
    }

    /**
     * Register ACF fields
     */
    public function productsAcfFields()
    {
        if (function_exists('acf_add_local_field_group')) :
            acf_add_local_field_group(array(
                'key' => 'group_5d0b7b768e612',
                'title' => 'Products',
                'fields' => array(
                    array(
                        'key' => $this->fields_key[0],
                        'label' => 'General',
                        'name' => '',
                        'type' => 'tab',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'placement' => 'top',
                        'endpoint' => 0,
                    ),
                    array(
                        'key' => $this->fields_key[1],
                        'label' => 'Author',
                        'name' => 'author',
                        'type' => 'user',
                        'instructions' => '',
                        'required' => 1,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '25',
                            'class' => '',
                            'id' => '',
                        ),
                        'role' => '',
                        'allow_null' => 0,
                        'multiple' => 0,
                        'return_format' => 'id',
                    ),
                    array(
                        'key' => $this->fields_key[2],
                        'label' => 'Information',
                        'name' => '',
                        'type' => 'tab',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'placement' => 'top',
                        'endpoint' => 0,
                    ),
                    array(
                        'key' => $this->fields_key[3],
                        'label' => 'Description',
                        'name' => 'description',
                        'type' => 'wysiwyg',
                        'instructions' => '',
                        'required' => 1,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'default_value' => '',
                        'tabs' => 'visual',
                        'toolbar' => 'basic',
                        'media_upload' => 1,
                        'delay' => 0,
                    ),
                    array(
                        'key' => $this->fields_key[4],
                        'label' => 'Images',
                        'name' => 'images',
                        'type' => 'image',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'return_format' => 'array',
                        'preview_size' => 'medium',
                        'library' => 'all',
                        'min_width' => '',
                        'min_height' => '',
                        'min_size' => '',
                        'max_width' => '',
                        'max_height' => '',
                        'max_size' => '',
                        'mime_types' => '',
                    ),
                    array(
                        'key' => $this->fields_key[5],
                        'label' => 'Price',
                        'name' => '',
                        'type' => 'tab',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'placement' => 'top',
                        'endpoint' => 0,
                    ),
                    array(
                        'key' => $this->fields_key[6],
                        'label' => 'Price',
                        'name' => 'price',
                        'type' => 'number',
                        'instructions' => '',
                        'required' => 1,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '25',
                            'class' => '',
                            'id' => '',
                        ),
                        'default_value' => 0,
                        'placeholder' => '',
                        'prepend' => '',
                        'append' => '',
                        'min' => '',
                        'max' => '',
                        'step' => '',
                    ),
                    array(
                        'key' => $this->fields_key[7],
                        'label' => 'Currency',
                        'name' => 'currency',
                        'type' => 'radio',
                        'instructions' => '',
                        'required' => 1,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'choices' => array(
                            '0' => 'UAH',
                            '1' => 'USD',
                            '2' => 'EUR',
                        ),
                        'allow_null' => 0,
                        'other_choice' => 0,
                        'default_value' => '',
                        'layout' => 'vertical',
                        'return_format' => 'value',
                        'save_other_choice' => 0,
                    ),
                ),
                'location' => array(
                    array(
                        array(
                            'param' => 'post_type',
                            'operator' => '==',
                            'value' => 'products',
                        ),
                    ),
                ),
                'menu_order' => 0,
                'position' => 'normal',
                'style' => 'seamless',
                'label_placement' => 'top',
                'instruction_placement' => 'label',
                'hide_on_screen' => '',
                'active' => true,
                'description' => '',
            ));
        endif;
    }

    /**
     * Save ACF actions
     */
    public function saveActions()
    {
        add_filter('acf/update_value', [$this, 'updateProduct'], 10, 3);
        add_filter('acf/load_value', [$this, 'loadProduct'], 10, 3);
    }

    /**
     * @param $value
     * @param $product_id
     * @param $field
     *
     * @return null
     */
    public function updateProduct($value, $product_id, $field)
    {
        if (in_array($field['key'], $this->fields_key)) {
            $user_id = wp_get_current_user()->ID;
            $product = \MaterialTheme\Models\Products::where([
                ['product', $product_id],
                ['author', $user_id],
            ])->first();

            if (!$product) {
                $product = \MaterialTheme\Models\Products::create([
                    'product' => $product_id,
                    'author' => $user_id
                ]);
            }

            switch ($field['name']) {
                case 'author':
                    $product->author = $value;
                    break;

                case 'description':
                    $product->desc = $value;
                    break;

                case 'images':
                    $product->images = $value;
                    break;

                case 'price':
                    $product->price = $value;
                    break;

                case 'currency':
                    $product->currency = $value;
                    break;
            }

            $product->save();
        }

        return null;
    }

    /**
     * @param $value
     * @param $product_id
     * @param $field
     *
     * @return string
     */
    public function loadProduct($value, $product_id, $field)
    {
        if (!in_array($field['key'], $this->fields_key)) {
            return $value;
        }

        $user_id = wp_get_current_user()->ID;
        $product = \MaterialTheme\Models\Products::where([
            ['product', $product_id],
            ['author', $user_id],
        ])->first();

        if (!$product) {
            return '';
        }

        switch ($field['name']) {
            case 'author':
                $data = $product->author;
                break;

            case 'description':
                $data = $product->desc;
                break;

            case 'images':
                $data = $product->images;
                break;

            case 'price':
                $data = $product->price;
                break;

            case 'currency':
                $data = $product->currency;
                break;

            default:
                $data = '';
                break;
        }

        return $data;
    }
}
