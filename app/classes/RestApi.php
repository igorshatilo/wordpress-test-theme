<?php

namespace MaterialTheme\Classes;

use MaterialTheme\Models\UserNotes;

/**
 * Class RestApi
 * @package MaterialTheme\Classes
 */
class RestApi extends \WP_REST_Controller
{
    /**
     * @var string
     */
    protected $namespace = 'user-notes';

    /**
     * RestApi constructor.
     */
    public function __construct()
    {
        add_action('rest_api_init', [$this, 'restApiInit']);
    }

    /**
     * Initialization of REST API
     */
    public function restApiInit()
    {
        register_rest_route($this->namespace, 'get', array(
            'methods'  => 'GET',
            'callback' => [$this, 'userNotesGetAll'],
            'permission_callback' => 'is_user_logged_in',
        ));

        register_rest_route($this->namespace, 'get/(?P<note>[\d]+)', array(
            'methods'  => 'GET',
            'callback' => [$this, 'userNotesGetOne'],
            'permission_callback' => [$this, 'userNotesAuthorCheck'],
        ));

        register_rest_route($this->namespace, 'search', array(
            'methods'  => 'GET',
            'callback' => [$this, 'userNotesSearch'],
            'permission_callback' => 'is_user_logged_in',
            'args' => [
                'q' => [
                    'type' => 'string',
                    'required' => true,
                ],
            ]
        ));

        register_rest_route($this->namespace, 'create', array(
            'methods'  => 'POST',
            'callback' => [$this, 'userNotesCreate'],
            'permission_callback' => 'is_user_logged_in',
            'args' => [
                'text' => [
                    'type' => 'string',
                    'required' => true,
                ],
            ]
        ));

        register_rest_route($this->namespace, 'edit', array(
            'methods'  => 'POST',
            'callback' => [$this, 'userNotesEdit'],
            'permission_callback' => [$this, 'userNotesAuthorCheck'],
            'args' => [
                'note' => [
                    'type' => 'integer',
                    'minimum' => 1,
                    'required' => true,
                ],
                'text' => [
                    'type' => 'string',
                    'required' => true,
                ],
            ]
        ));

        register_rest_route($this->namespace, 'delete/(?P<note>[\d]+)', array(
            'methods'  => 'POST',
            'callback' => [$this, 'userNotesDelete'],
            'permission_callback' => [$this, 'userNotesAuthorCheck']
        ));


        register_rest_route($this->namespace, 'check-login', array(
            'methods'  => 'POST',
            'callback' => [$this, 'userNotesCheckAuth']
        ));

        register_rest_route($this->namespace, 'login', array(
            'methods'  => 'POST',
            'callback' => [$this, 'userNotesAuth'],
            'args' => [
                'login' => [
                    'type' => 'string',
                    'required' => true,
                ],
                'password' => [
                    'type' => 'string',
                    'required' => true,
                ],
            ]
        ));
    }

    /**
     * @return false|mixed|string|void
     */
    public function userNotesCheckAuth()
    {
        if (!empty(wp_get_current_user()->ID)) {
            return json_encode([
                'error' => false
            ]);
        } else {
            return json_encode([
                'error' => true
            ]);
        }
    }

    /**
     * @param \WP_REST_Request $request
     *
     * @return false|mixed|string|void
     */
    public function userNotesAuth(\WP_REST_Request $request)
    {
        $creds['user_login'] = $request->get_param('login');
        $creds['user_password'] = $request->get_param('password');

        $user = wp_signon($creds, false);

        if (is_wp_error($user)) {
            return json_encode([
                'error' => true,
                'message' => $user->get_error_message()
            ]);
        } else {
            return json_encode([
                'error' => false
            ]);
        }
    }

    /**
     * @return false|mixed|string|void
     */
    public function userNotesGetAll()
    {
        $user_id = wp_get_current_user()->ID;
        $users_notes = UserNotes::where([
            ['user_id', $user_id],
        ]);

        return json_encode($users_notes);
    }

    /**
     * @param \WP_REST_Request $request
     *
     * @return false|mixed|string|void
     */
    public function userNotesGetOne(\WP_REST_Request $request)
    {
        $users_notes = UserNotes::find($request->get_param('id'))->toArray() ? : [];
        return json_encode($users_notes);
    }

    /**
     * @param \WP_REST_Request $request
     *
     * @return string
     */
    public function userNotesCreate(\WP_REST_Request $request)
    {
        $user_id = wp_get_current_user()->ID;
        $text = $request->get_param('text');

        if ($user_id and $text) {
            $note = UserNotes::create([
                'user_id' => $user_id,
                'text' => $text
            ]);

            return $note;
        }

        return 'Error.';
    }

    /**
     * @param \WP_REST_Request $request
     *
     * @return string
     */
    public function userNotesEdit(\WP_REST_Request $request)
    {
        $user_id = wp_get_current_user()->ID;
        $note_id = (int) $request->get_param('note');
        $text = $request->get_param('text');

        if ($user_id and $note_id and $text) {
            $note = UserNotes::where([
                ['id', $note_id],
                ['user_id', $user_id],
                ['updated_at', date('Y-m-d G:i:s', time())],
            ])->first();

            if ($note) {
                $note->text = $text;
                $note->save();

                return $note;
            }
        }

        return 'Error.';
    }

    /**
     * @param \WP_REST_Request $request
     *
     * @return string
     */
    public function userNotesDelete(\WP_REST_Request $request)
    {
        $user_id = wp_get_current_user()->ID;
        $note_id = (int) $request->get_param('note');

        $note = UserNotes::where([
            ['id', $note_id],
            ['user_id', $user_id],
        ])->first();

        if ($note) {
            $note->delete();

            return $note;
        }

        return 'Error.';
    }

    /**
     * @param \WP_REST_Request $request
     *
     * @return bool
     */
    public function userNotesAuthorCheck(\WP_REST_Request $request)
    {
        $note_id = $request->get_param('note');

        if (is_user_logged_in()) {
            $user_id = wp_get_current_user()->ID;

            $note = UserNotes::where([
                ['id', $note_id],
                ['user_id', $user_id],
            ])->first();

            if ($note) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param \WP_REST_Request $request
     *
     * @return false|mixed|string|void
     */
    public function userNotesSearch(\WP_REST_Request $request)
    {
        $q = $request->get_param('q');
        $user_id = wp_get_current_user()->ID;

        $notes = UserNotes::where([
            ['user_id', $user_id],
            ['text', 'like', '%' . $q . '%'],
        ])->get();

        return json_encode($notes);
    }
}
