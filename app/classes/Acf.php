<?php

namespace MaterialTheme\Classes;

use MaterialTheme\Models\UserNotes;
use WeDevs\ORM\WP\User;

/**
 * Class Acf
 * @package MaterialTheme\Classes
 */
class Acf
{
    /**
     * Acf constructor.
     */
    public function __construct()
    {
        add_action('acf/init', [ $this, 'myAcfInit' ]);
        add_filter('acf/load_value', [ $this, 'loadUserNote' ], 11, 3);
        add_filter('acf/update_value', [ $this, 'updateUserNote' ], 11, 3);
    }

    /**
     * Initialization of ACF
     */
    public function myAcfInit()
    {
        if (function_exists('acf_add_local_field_group')) :
            acf_add_local_field_group(array(
                'key'                   => 'group_5d0b3f9ef1993',
                'title'                 => 'User Notes',
                'fields'                => array(
                    array(
                        'key'               => 'field_5d0b3f76caaaa',
                        'label'             => 'Note',
                        'name'              => 'user_note',
                        'type'              => 'text',
                        'instructions'      => '',
                        'required'          => 0,
                        'conditional_logic' => 0,
                        'wrapper'           => array(
                            'width' => '',
                            'class' => '',
                            'id'    => '',
                        ),
                        'default_value'     => '',
                        'placeholder'       => '',
                        'prepend'           => '',
                        'append'            => '',
                        'maxlength'         => '',
                    ),
                ),
                'location'              => array(
                    array(
                        array(
                            'param'    => 'user_form',
                            'operator' => '==',
                            'value'    => 'all',
                        ),
                    ),
                ),
                'menu_order'            => 0,
                'position'              => 'normal',
                'style'                 => 'default',
                'label_placement'       => 'top',
                'instruction_placement' => 'label',
                'hide_on_screen'        => '',
                'active'                => true,
                'description'           => '',
            ));
        endif;
    }

    /**
     * @param $value
     * @param $user_id
     * @param $field
     *
     * @return string
     */
    public function loadUserNote($value, $user_id, $field)
    {
        if ($field['name'] != 'user_note') {
            return $value;
        }

        $user_id = explode('_', $user_id)[1];
        $data    = $this->getUserNote($user_id);

        if (empty($data)) {
            return '';
        }

        return $data;
    }

    /**
     * @param $user_id
     *
     * @return bool|string
     */
    public function getUserNote($user_id)
    {
        $user = User::find($user_id);

        if ($user) {
            $user_note = UserNotes::where([
                [ 'user_id', $user_id ]
            ])->first();

            if ($user_note) {
                return $user_note->text;
            }

            return '';
        }

        return false;
    }

    /**
     * @param $value
     * @param $user_id
     * @param $field
     *
     * @return null
     */
    public function updateUserNote($value, $user_id, $field)
    {
        if ('user_note' == $field['name']) {
            $user_id = explode('_', $user_id)[1];
            $user    = User::find($user_id);

            if ($user) {
                $user_note = UserNotes::where([
                    [ 'user_id', $user_id ]
                ])->first();

                if ($user_note) {
                    $user_note->text = $value;
                    $user_note->save();
                } else {
                    $user_note = UserNotes::create([
                        'user_id' => $user_id,
                        'text'    => $value
                    ]);
                }
            }

            return null;
        }

        return $value;
    }
}
