<?php

namespace MaterialTheme\Classes;

/**
 * Class Theme
 * @package MaterialTheme\Classes
 */
class Theme
{
    const CSS_PREFIX = 'material-theme-css';
    const JS_PREFIX = 'material-theme-js';

    /**
     * Theme constructor.
     */
    public function __construct()
    {
        add_action('wp_enqueue_scripts', [ $this, 'loadStyles' ]);
        add_action('wp_enqueue_scripts', [ $this, 'loadScripts' ]);

        $this->themeSupport();
    }

    /**
     * Load CSS files.
     */
    public function loadStyles()
    {
        $stylesDir = get_template_directory() . '/assets/css/';
        $styles    = array_slice(scandir($stylesDir), 2);

        foreach ($styles as $key => $cssFile) {
            wp_enqueue_style(self::CSS_PREFIX . $key, get_template_directory_uri() . '/assets/css/' . $cssFile);
        }
    }

    /**
     * Load JS files.
     */
    public function loadScripts()
    {
        $scriptsDir = get_template_directory() . '/assets/js/';
        $scripts    = array_slice(scandir($scriptsDir), 2);

        foreach ($scripts as $key => $jsFile) {
            wp_enqueue_script(self::JS_PREFIX . $key, get_template_directory_uri() . '/assets/js/' . $jsFile);
        }
    }

    /**
     * Added theme support
     */
    public function themeSupport()
    {
        add_theme_support('post-thumbnails');
        add_theme_support('menus');
        add_theme_support('widgets');
        add_theme_support('title-tag');
    }
}
