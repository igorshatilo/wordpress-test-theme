<?php get_header(); ?>
<?php
if ( have_posts() ) :

	while ( have_posts() ) :
		the_post();
		?>
        <a href="<?= get_the_permalink(); ?>"><h2><?= get_the_title(); ?></h2></a>
        <p><?= get_the_excerpt(); ?></p>
	<?php
	endwhile;

	the_posts_navigation();

else :
	?>

<?php
endif;
?>
<?php get_footer(); ?>